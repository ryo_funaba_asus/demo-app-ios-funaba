
# 概要
内定者インターンで作成したiOSのデモアプリです。


# 設計書
[こちら](https://sonix-asia.atlassian.net/wiki/download/attachments/372834343/FY2020%20%E3%82%A4%E3%83%B3%E3%82%BF%E3%83%BC%E3%83%B3%E5%AE%9F%E8%B7%B5%E8%AA%B2%E9%A1%8C%E3%80%80%E3%83%86%E3%82%99%E3%83%A2%E3%82%A2%E3%83%95%E3%82%9A%E3%83%AA%E7%94%BB%E9%9D%A2%E8%A8%AD%E8%A8%88%E6%9B%B8.xlsx?api=v2)から.xlxsファイルとしてダウンロードできます


# 実装した画面
- TOP画面
- 詳細画面
	- Picker画面
	- Map画面
	- Video画面
	- Web View画面
	- View Pager画面
	- Form画面


# 画面スクショ
- TOP画面

![photo](https://user-images.githubusercontent.com/59598693/108029016-18d21000-7070-11eb-9c6b-7fc41170964b.png =50%x50%)

- 詳細画面

![photo](https://user-images.githubusercontent.com/59598693/108029024-1c659700-7070-11eb-96ff-571e7f4b46b7.png =50%x50%)

- Picker画面
- Map画面
- Video画面
- Web View画面
- View Pager画面
- Form画面


# 機能
- TOP画面
	- 各リストの詳細画面に遷移できる機能

- Picker画面
- Map画面
- Video画面
- Web View画面
- View Pager画面
- Form画面

