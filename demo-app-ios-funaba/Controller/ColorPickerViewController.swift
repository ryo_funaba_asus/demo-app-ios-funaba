//
//  ColorPickerViewController.swift
//  demo-app-ios-funaba
//
//  Created by rfunaba on 2021/02/17.
//

import UIKit

final class ColorPickerViewController: UIViewController {
  @IBOutlet private weak var colorArea: UIView!
  
  private var redValue: CGFloat = 0 {
    didSet {
      updateBackgroundColor()
    }
  }
  private var greenValue: CGFloat = 0 {
    didSet {
      updateBackgroundColor()
    }
  }
  private var blueValue: CGFloat = 0 {
    didSet {
      updateBackgroundColor()
    }
  }
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }

  @IBAction func setRedValue(_ sender: UISlider) {
    redValue = CGFloat(sender.value)
  }

  @IBAction func setGreenValue(_ sender: UISlider) {
    greenValue = CGFloat(sender.value)
  }
  
  @IBAction func setBlueValue(_ sender: UISlider) {
    blueValue = CGFloat(sender.value)
  }
  
  private func updateBackgroundColor() {
    colorArea.backgroundColor = makeRGBColor()
  }
  
  private func makeRGBColor() -> UIColor {
    UIColor(red: redValue/255, green: greenValue/255, blue: blueValue/255, alpha: 1)
  }
}
