//
//  FormViewController.swift
//  demo-app-ios-funaba
//
//  Created by rfunaba on 2021/03/09.
//

import UIKit

final class FormViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    private var name = "（未入力）"
    private var gender: genderType = .man
    
    enum genderType: Int{
        case man
        case woman
        case other
        
        var conversionToString: String {
            switch self {
            case .man:
                return "男性"
            case .woman:
                return "女性"
            case .other:
                return "その他"
            }
        }
    }
    private var birthday = "（未入力）"
    private var yearArray = Array(1900...2021)
    private var monthArray = Array(1...12)
    private var dayArray = Array(1...31)
    private var age: String!
    private var pickerDataArray: Array<Array<String>> = []
    private var checkedButtons: Array<sourceOfInformationType> = []
    
    enum sourceOfInformationType: Int {
        case internet = 3
        case comic
        case friend
        case seminar
        case other
        
        var conversionToString: String {
            switch self {
            case .internet:
                return "インターネット"
            case .comic:
                return "雑誌記事"
            case .friend:
                return "友人/知人から"
            case .seminar:
                return "セミナー"
            case .other:
                return "その他"
            }
        }
    }
    // ラジオボタンとチェックボックスで使用する画像を取得
    private let selectedImage = UIImage(systemName: "circle.circle.fill")! as UIImage
    private let unselectedImage = UIImage(systemName: "circle")! as UIImage
    private let checkedImage = UIImage(systemName: "checkmark.square")! as UIImage
    private let uncheckedImage = UIImage(systemName: "square")! as UIImage
    
    @IBOutlet weak var manButton: UIButton!
    @IBOutlet weak var womanButton: UIButton!
    @IBOutlet weak var otherGenderButton: UIButton!
    @IBOutlet weak var yearField: UITextField!
    @IBOutlet weak var monthField: UITextField!
    @IBOutlet weak var dayField: UITextField!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var internetButton: UIButton!
    @IBOutlet weak var comicButton: UIButton!
    @IBOutlet weak var friendButton: UIButton!
    @IBOutlet weak var seminarButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    @IBAction func changedName(_ sender: UITextField) {
        guard let nameText = sender.text else {
            return
        }
        name = nameText
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArray[pickerView.tag].count
    }
    
    internal func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataArray[pickerView.tag][row]
    }
    
    internal func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            yearField.text = pickerDataArray[pickerView.tag][row]
        } else if pickerView.tag == 1 {
            monthField.text = pickerDataArray[pickerView.tag][row]
        } else if pickerView.tag == 2 {
            dayField.text = pickerDataArray[pickerView.tag][row]
        }
        
        birthday = yearField.text! + "年" + monthField.text! + "月" + dayField.text! + "日"
        calculateAge()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setRadioButton()
        setPickerView()
        setCheckButton()
    }
    
    private func setRadioButton() {
        manButton.tag = 0
        womanButton.tag = 1
        otherGenderButton.tag = 2
        
        // 各ラジオボタンに、クリック時に実行する関数を定義
        for num in (0...2) {
            for v in view.subviews {
                if let v = v as? UIButton, v.tag == num {
                    v.addTarget(self, action: #selector(buttonSelected(_:)), for: UIControl.Event.touchUpInside)
                }
            }
        }
    }
    
    @objc private func buttonSelected (_ sender: UIButton) {
        let button = sender
        if button.tag <= 2 {
            ChangeToUnchecked()
            button.setImage(selectedImage, for: .normal)
            
            gender = genderType(rawValue: button.tag)!
        }
    }
    
    private func ChangeToUnchecked() {
        for v in view.subviews {
            if let v = v as? UIButton, v.tag == gender.rawValue {
                v.setImage(unselectedImage, for: .normal)
            }
        }
    }
    
    private func setPickerView() {
        createPickerView(yearField, 0)
        createPickerView(monthField, 1)
        createPickerView(dayField, 2)
        
        let stringYear = yearArray.map{ String($0)}
        let stringMonth = monthArray.map{ String($0)}
        let stringDay = dayArray.map{ String($0)}
        
        pickerDataArray.append(stringYear)
        pickerDataArray.append(stringMonth)
        pickerDataArray.append(stringDay)
        
        // YearのPickerのデフォルト値を設定
        yearField.addTarget(self, action: #selector(setDefalutYear(_:)), for: .editingDidBegin)
    }
    
    // 年・月・日のフィールドのPickerを設定する
    private func createPickerView(_ field: UITextField, _ tag: Int) {
        let pickerView = UIPickerView()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = tag
        
        field.inputView = pickerView
        
        // ツールバーの設定
        let toolbar = UIToolbar()
        toolbar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44)
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(FormViewController.donePicker))
        toolbar.setItems([doneButtonItem], animated: true)
        
        field.inputAccessoryView = toolbar
    }
    
    @objc private func donePicker() {
        yearField.endEditing(true)
        monthField.endEditing(true)
        dayField.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        yearField.endEditing(true)
        monthField.endEditing(true)
        dayField.endEditing(true)
    }
    
    private func calculateAge() {
        let birthdate = DateComponents(
            year: Int(yearField.text!),
            month: Int(monthField.text!),
            day: Int(dayField.text!)
        )
        
        // 年齢を算出する
        let calendar = Calendar.current
        let now = calendar.dateComponents([.year, .month, .day], from: Date())
        let ageComponents = calendar.dateComponents([.year], from: birthdate, to: now)
        age = String(ageComponents.year!)
        
        ageLabel.text = age + "歳"
    }
    
    @objc func setDefalutYear(_ textField: UITextField) {
        (textField.inputView as? UIPickerView)?.selectRow(90, inComponent: 0, animated: false)
    }
    
    private func setCheckButton() {
        internetButton.tag = 3
        comicButton.tag = 4
        friendButton.tag = 5
        seminarButton.tag = 6
        otherButton.tag = 7
        
        // 各チェックボタンに、クリック時に実行する関数を定義
        for num in (3...7) {
            for v in view.subviews {
                if let v = v as? UIButton, v.tag == num {
                    v.addTarget(self, action: #selector(buttonChecked(_:)), for: UIControl.Event.touchUpInside)
                }
            }
        }
    }
    
    @objc private func buttonChecked (_ sender: UIButton) {
        let button = sender
        
        guard button.tag >= 3, button.tag <= 7 else {
            return
        }
        
        let buttonTag = sourceOfInformationType(rawValue: button.tag)!
  
        // チェック済みのボタンはcheckedButtons配列に入れることとする
        if checkedButtons.contains(buttonTag) {
            button.setImage(uncheckedImage, for: .normal)
            checkedButtons.removeAll(where: {$0 == buttonTag})
        } else {
            button.setImage(checkedImage, for: .normal)
            checkedButtons.append(buttonTag)
        }
    }
    
    // 結果画面への遷移時に、遷移先のコントローラーにデータを渡す
    internal override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFormAnswerView" {
            let nextView = segue.destination as! FormAnswerViewController
            
            nextView.name = name
            nextView.gender = gender.conversionToString
            nextView.birthday = birthday
            nextView.age = age ?? "（未入力）"
            nextView.sourceOfInformation = checkedButtons.map { $0.conversionToString }.reduce(""){ $0 + "\($1), " }
        }
    }
}
