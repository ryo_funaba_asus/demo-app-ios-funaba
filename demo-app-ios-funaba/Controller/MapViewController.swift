//
//  MapViewController.swift
//  demo-app-ios-funaba
//
//  Created by rfunaba on 2021/02/19.
//

import UIKit
import GoogleMaps

final class MapViewController: UIViewController {
    
    private var mapView = GMSMapView()
    private let defaultPosition = CLLocationCoordinate2D(latitude: 35.62336408457903, longitude: 139.7199318533382)
    private let defaultZomm = 16.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefaultPosition()
        // マップの中心にマーカーをセットする
        setDefaultMakar()
    }
    
    private func setDefaultPosition() {
        let camera = GMSCameraPosition.camera(withTarget: defaultPosition, zoom: Float(defaultZomm))
        
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        // 右下にある、現在地に遷移するボタンを有効化
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        addMapViewToSubView()
    }
    
    private func setDefaultMakar() {
        let marker = GMSMarker()
        marker.position = defaultPosition
        marker.title = "株式会社ソニックス"
        marker.snippet = "サテライトオフィス"
        marker.map = mapView
    }
    
    private func addMapViewToSubView() {
        self.view.addSubview(mapView)
        self.view.bringSubviewToFront(mapView)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
