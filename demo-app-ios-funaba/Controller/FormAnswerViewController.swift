//
//  FormAnswerViewController.swift
//  demo-app-ios-funaba
//
//  Created by rfunaba on 2021/03/11.
//

import UIKit

final class FormAnswerViewController: UIViewController {

    public var name: String!
    public var gender: String!
    public var birthday: String!
    public var age: String!
    public var sourceOfInformation: String!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var sourceOfInformationLabel: UILabel!
    
    @IBAction func clickedToTopButton(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = name
        genderLabel.text = gender
        birthdayLabel.text = birthday
        ageLabel.text = age + "歳"
        sourceOfInformationLabel.text = sourceOfInformation
    }
}
