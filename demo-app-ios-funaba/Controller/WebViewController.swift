import UIKit
// 1 WebKit の import
import WebKit

final class WebViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    private var webView: WKWebView!
    
    override func loadView() {
        createWebView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestUrl()
    }
    
    internal func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webView.evaluateJavaScript(
            "document.title"
        ) { (result, error) -> Void in
            self.navigationItem.title = result as? String
        }
    }
    
    private func createWebView() {
        // 2 WKWebViewConfiguration の生成
        let webConfiguration = WKWebViewConfiguration()
        // 3 WKWebView に Configuration を引き渡し initialize
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        // 4 WKUIDelegate の移譲先として self を登録
        webView.uiDelegate = self
        // 5 WKNavigationDelegate の移譲先として self を登録
        webView.navigationDelegate = self
        // 6 view に webView を割り当て
        view = webView
    }
    
    private func requestUrl() {
        // 7 URLオブジェクトを生成
        let myURL = URL(string:"https://www.sonix.asia/")
        // 8 URLRequestオブジェクトを生成
        let myRequest = URLRequest(url: myURL!)
        // 9 URLを WebView にロード
        webView.load(myRequest)
    }
}

