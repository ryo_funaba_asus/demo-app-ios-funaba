//
//  MemoTableViewController.swift
//  demo-app-ios-funaba
//
//  Created by rfunaba on 2021/02/16.
//

import UIKit

class MemoTableViewController: UITableViewController {

    let ScreenName = ["ピッカー画面", "マップ画面", "WEB画面", "ページャー画面", "ビデオ画面", "フォーム画面"]
  
    enum ScreenNames: Int {
        case colorPickerView = 0
        case MapView
        case WebView
        case PagerView
        case VideoView
        case FormView
        
        var getSeugeIdentifier: String {
            switch self {
            case .colorPickerView:
                return "toColorPickerView"
            case .MapView:
                return "toMapView"
            case .WebView:
                return  "toWebView"
            case .PagerView:
                return "toPagerView"
            case .VideoView:
                return "toVideoView"
            case .FormView:
                return "toFormView"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ナビゲーションバーの文字色を白に変更
        self.navigationController?.navigationBar.tintColor = UIColor.white;
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.ScreenName.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemoTableViewCell", for: indexPath)

        cell.textLabel?.text = self.ScreenName[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case ScreenNames.colorPickerView.rawValue:
            self.performSegue(withIdentifier: ScreenNames.colorPickerView.getSeugeIdentifier, sender: nil)
        case ScreenNames.MapView.rawValue:
            self.performSegue(withIdentifier: ScreenNames.MapView.getSeugeIdentifier, sender: nil)
        case ScreenNames.WebView.rawValue:
            self.performSegue(withIdentifier: ScreenNames.WebView.getSeugeIdentifier, sender: nil)
        case ScreenNames.PagerView.rawValue:
            self.performSegue(withIdentifier: ScreenNames.PagerView.getSeugeIdentifier, sender: nil)
        case ScreenNames.VideoView.rawValue:
            self.performSegue(withIdentifier: ScreenNames.VideoView.getSeugeIdentifier, sender: nil)
        case ScreenNames.FormView.rawValue:
            self.performSegue(withIdentifier: ScreenNames.FormView.getSeugeIdentifier, sender: nil)
        default:
            return
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier != nil else {
        return
      }
    }
}
