//
//  VideoViewController.swift
//  demo-app-ios-funaba
//
//  Created by rfunaba on 2021/03/04.
//

import UIKit
import AVKit

final class VideoViewController: UIViewController {
    
    private var isPlaying = false {
        didSet {
            changeBtnTitle()
        }
    }
    private var isHidden = false {
        didSet {
            changePlayBtnsHideShow()
        }
    }
    private var player: AVPlayer!
    private var url: URL!
    private var timeObserver: Any!
    private var playBackTime: Float64!
    
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var seekBar: UISlider!
    @IBOutlet weak var currentPlayBackTimeLabel: UILabel!
    @IBOutlet weak var playBackTimeLabel: UILabel!
    @IBOutlet weak var BackgroundViewOfBtns: UIView!
    
    @IBAction func tapPlayBtn(_ sender: UIButton) {
        playVideo()
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        // 動画の再生時間をシークバーとシンクロさせる.
        player.seek(to: CMTimeMakeWithSeconds(Float64(seekBar.value),
                                              preferredTimescale: Int32(NSEC_PER_SEC)))
    }
    
    @objc private func rotationChange(notification: NSNotification){
        isHidden = false
        setVideoToUiView()
    }
    
    @objc private func uiViewTapped(_ sender:UITapGestureRecognizer) {
        if UIDevice.current.orientation.isLandscape {
            isHidden = !isHidden
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setVideoToUiView()
        setSeekBar(url)
        setPlayBackTimeLabel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.rotationChange(notification:)),
                                               name:UIDevice.orientationDidChangeNotification,
                                               object: nil)
    }
    
    private func setVideoToUiView() {
        let fileName = "clip"
        let fileExtension = "mp4"
        
        url = Bundle.main.url(forResource: fileName, withExtension: fileExtension)
        
        guard url != nil else {
            print("Url is nil")
            return
        }
        
        // AVPlayerにアイテムをセット
        let item = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: item)
        
        // AVPlayerLayerにAVPlayerをセット
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.uiView.bounds
        playerLayer.videoGravity = .resizeAspectFill
        
        self.uiView.layer.sublayers?[0].removeFromSuperlayer()
        self.uiView.layer.insertSublayer(playerLayer, at: 0)
        
        // uiViewにタップを検知するジェスチャーを追加
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.uiViewTapped(_:)))
        self.uiView.addGestureRecognizer(gesture)
    }
    
    private func setSeekBar(_ url: URL) {
        let asset = AVAsset(url: url)
        seekBar.minimumValue = 0
        seekBar.maximumValue = Float(CMTimeGetSeconds(asset.duration))
        seekBar.addTarget(self, action: #selector(sliderValueChanged(_:)), for: UIControl.Event.valueChanged)
        
        // time毎に呼び出される.
        setTimeObserver()
    }
    
    private func setPlayBackTimeLabel() {
        // 総再生時間を取得する
        playBackTime = CMTimeGetSeconds(self.player.currentItem!.asset.duration)
        
        currentPlayBackTimeLabel.text = "00:00"
        playBackTimeLabel.text = formatPlayTime(playBackTime)
    }
    
    private func setTimeObserver() -> Void {
        // x分割で動かす事が出来る様にインターバルを指定.
        let interval = Double(5.0 * seekBar.maximumValue) / Double(seekBar.bounds.maxX)
        // CMTimeに変換する.
        let time: CMTime = CMTimeMakeWithSeconds(interval, preferredTimescale: Int32(NSEC_PER_SEC))
        
        player.addPeriodicTimeObserver(forInterval: time, queue: .main, using: { time in
            // 現在の時間を取得
            let time = CMTimeGetSeconds(self.player.currentTime())
            
            // シークバーの位置を変更.
            let value = Float(self.seekBar.maximumValue) * Float(time) / Float(self.playBackTime) + Float(self.seekBar.minimumValue)
            
            self.seekBar.value = value
            
            self.currentPlayBackTimeLabel.text = self.formatPlayTime(Float64(time))
        })
    }
    
    private func formatPlayTime(_ seconds: Float64) -> String{
        let Min = Int(seconds / 60)
        let Sec = Int(seconds.truncatingRemainder(dividingBy: 60))
        return String(format: "%02d:%02d", Min, Sec)
    }
    
    private func playVideo() {
        if isPlaying == true {
            player.pause()
        } else if isPlaying == false {
            player.play()
        }
        
        isPlaying = !isPlaying
    }
    
    private func changeBtnTitle() {
        if isPlaying == false {
            playBtn.setTitle("▶︎", for: .normal)
        } else if isPlaying == true {
            playBtn.setTitle("■", for: .normal)
        }
    }
    
    private func changePlayBtnsHideShow() {
        BackgroundViewOfBtns.isHidden = isHidden
        playBtn.isHidden = isHidden
        seekBar.isHidden = isHidden
        currentPlayBackTimeLabel.isHidden = isHidden
        playBackTimeLabel.isHidden = isHidden
        navigationController?.setNavigationBarHidden(isHidden, animated: false)
    }
}
