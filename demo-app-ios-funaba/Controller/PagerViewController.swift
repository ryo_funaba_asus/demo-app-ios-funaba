//
//  PagerViewController.swift
//  demo-app-ios-funaba
//
//  Created by rfunaba on 2021/02/24.
//

import UIKit

class PagerViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    let images = ["dog_1", "dog_2"]
    var pageNumber: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageNumber = images.count
        
        // スクリーンの縦横サイズを取得
        let screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = scrollView.bounds.height
        
        // scrollViewに画像を追加
        for i in 0 ..< pageNumber {
            let image = UIImage(named: images[i])!
            let imageView = UIImageView(image: image)
            
            imageView.frame = CGRect(x: 0,
                                     y: 0,
                                     width: screenWidth,
                                     height: screenHeight)
            
            imageView.tag = i + 1
            
            imageView.contentMode = .scaleAspectFit
                        
            self.scrollView.addSubview(imageView)
        }
        
        setupScrollImages()
    }
    
    private func setupScrollImages(){
        // ダミー画像
        let imageDummy = UIImage(named: images[0])!
        var imageView = UIImageView(image: imageDummy)
        let subviews = scrollView.subviews
        
        // 描画開始の x,y 位置
        var px: CGFloat = 0.0
        let py: CGFloat = 0.0
        
        // verticalScrollIndicatorとhorizontalScrollIndicatorが
        // デフォルトで入っているので2から始める
        // ここでimageViewのoriginを決める
        for i in 2 ..< subviews.count {
            imageView = subviews[i] as! UIImageView
            if imageView.isKind(of: UIImageView.self) && imageView.tag > 0 {
                
                imageView.frame.origin = CGPoint(x: px, y: py)
                
                px += (screenWidth)
            }
        }
        // UIScrollViewのコンテンツサイズを全画像の総横幅に合わせる
        let contentWidth = screenWidth * CGFloat(pageNumber)
        scrollView.contentSize = CGSize(width: contentWidth, height: screenHeight)
    }
}
